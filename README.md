
#Mastermind

##Summary

This is just a simple implementation of the game "Mastermind" I made in 2004, which nevertheless still works in modern browsers.

As the code is ancient and outdated, I'm releasing it into the public domain.

##Demo

For a live demo, go to [http://jslegers.github.com/mastermind/](http://jslegers.github.com/mastermind/).

This implementation is MIT licensed.